package com.nitesh.mongopoc.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.nitesh.mongopoc.model.Book;

public interface BookRepository extends MongoRepository<Book, String> {

	@Transactional
	@Query(value = "UPDATE book set author=:author where id=:id", nativeQuery = true)
	public void updateAuthorById(@Param("id") int id, @Param("author") String author);
}
